Docker build
  docker build -t ifarmtransfer .

Docker push to Repo
  docker push dwetter/ifarmtransfer

Docker start
  docker-compose up

Docker, connect to running container
  docker ps (lists running containers)
  docker exec -ti container_name /bin/bash
  docker container attach ifarmhouse_transfer

Docker, cleanup
  docker system prune -a
  docker builder prune -a

GIT
  Pull from Gitlab: git pull origin master
  Commit changes: git add . & git commit -m "message"
  Push to GitLab: git push origin master

Firebird on Mac
  ln -s /Library/Frameworks/firebird.framework/Versions/Current/Resources/lib/libfbclient.dylib /usr/local/lib/firebird.dylib

Restore Firebird Backup
  gbak -c -user ifarm -password 1234 ./Backup.fbk ./Manuel.fdb

*************************
Deploy

1) Push the code changes to GitLab
-> Commit code in PyCharm

2) Connect to Raspberry and get the new code
git pull
2a) in case of pull error, delete the local files
rm config/config_prod.ini

3) Build docker on raspberry
docker-compose build

4) Check, if docker is already running
docker ps
docker kill ifarmhouse_transfer

5) Start docker on raspberry
docker-compose up




*************************
Manuel Access
  133 992 488
  gkgihz6p