import fdb
from fdb import Cursor

def main(args):
    try:
        print("Hello Cow!")
        print("Connecting...")
        con = fdb.connect(dsn="192.168.8.103:C:\Program Files (x86)\Saturnus\Database\Database.fdb", user="sysdba", password="masterkey")
        cur = con.cursor()
        print("Connected to DB ", con.database_name)
        print("Firebird version", con.firebird_version)
        
        cur.execute("SELECT ANIMALNR, IRNR, NAME, DATE_BIRTH, DELETED FROM TBL_ANIMAL;")
        rows = cur.fetchall()
        print("\n\Animal report \n")
        print("NR\tIRNR\t\tNAME\tDATE_BIRTH\tDELETED")
        for row in rows:
            print(str(row[0])+"\t"+str(row[1])+"\t"+str(row[2])+"\t"+str(row[3])+"\t"+str(row[4]))
        
        
    except BaseException as err:
        print("Unexpected Error: ",err)
            
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
