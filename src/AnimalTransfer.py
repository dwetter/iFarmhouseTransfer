from Animal import Animal
import json

class AnimalWithUpdate:
    femaleOnly = False
    lastSynch = "2021-06-21T12:21:43.3456057Z"
    speciesID = ""

class AnimalTransfer:
    remoteID = ""
    cloudID = ""
    updatePropertyList = ""
    animal = Animal()

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=2)

    def GetJSON(self):
        res = {}
        res["animal"] = self.animal.GetJSON()
        res["remoteID"] = self.remoteID
        res["cloudID"] = self.cloudID
        return res

    def InitFromJSON(self, transferJSON):
        a = Animal()
        a.InitFromJSON(transferJSON["animal"])
        self.animal = a
        self.remoteID = transferJSON["remoteID"]
        self.cloudID = transferJSON["cloudID"]