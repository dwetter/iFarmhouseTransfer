import fdb
from fdb import Cursor

def main(args):
    try:
        print("Hello Cow!")
        print("Connecting...")
        con = fdb.connect(dsn="192.168.8.103:C:\Program Files (x86)\Saturnus\Database\Database.fdb", user="sysdba", password="masterkey")
        cur = con.cursor()
        print("Connected to DB ", con.database_name)
        print("Firebird version", con.firebird_version)
        
        cur.execute("SELECT * FROM TBL_VERSIONINFO;")
        rows = cur.fetchall()
        print("\n\nVersion Info\n")
        for row in rows:
            print(str(row[2]), "\t", str(row[3]))
        
        cur.execute("SELECT first 20 * FROM TBL_SYSTEMLOG ORDER BY DATETIME desc;")
        rows = cur.fetchall()
        print("\n\nLast 20 Log entries\n")
        for row in rows:
            print(str(row[1]), "\t", str(row[2]), "\t", str(row[3]))
    except BaseException as err:
        print("Unexpected Error: ",err)
            
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
