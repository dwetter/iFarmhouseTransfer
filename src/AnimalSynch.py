import collections
import json
import fdb
import requests
import logging
import json
import os.path
from fdb import Cursor
from datetime import datetime, timezone
from Animal import Animal
from AnimalTransfer import AnimalWithUpdate, AnimalTransfer

class AnimalSynchMap:
    def __init__(self, intkey, cloudID, lastSynch):
        self.intkey = intkey
        self.cloudID = cloudID
        self.lastSynch = lastSynch

    @staticmethod
    def create_from_json(data):
        json_dictionary = json.loads(data)
        return AnimalSynchMap(**json_dictionary)

    def __str__(self):
        return f"<AnimalSynchMap {self.intkey} - {self.cloudID} - {self.lastSynch}>"

logger = logging.getLogger('AnimalUpdate')

def AnimalUpload(config_object):
    try:
        #Load SynchMap Data
        animalSynchDataFile = config_object["SYNCHDATA"]["animalsynchmap"]
        animalSynchMap = []
        if (os.path.isfile(animalSynchDataFile)):
            #load the last synch objects
            with open(animalSynchDataFile, "r") as json_data:
                animalsynchmaps_json = json.loads(json_data.read())

                for synchMapData in animalsynchmaps_json:
                    animalSynchMap.append(AnimalSynchMap(**synchMapData))

        url = config_object["SERVERCONFIG"]["url"]+config_object["SERVERCONFIG"]["animalupload"]
        token = config_object["FARMINFO"]["token"]
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain', 'Authorization': 'Bearer ' + token}
        #Initiate local DB connection
        dbdsn = config_object["DBCONFIG"]["dsn"]
        dbuser = config_object["DBCONFIG"]["user"]
        dbpassword = config_object["DBCONFIG"]["password"]
        logger.debug("All config values loaded")
        con = fdb.connect(dsn=dbdsn,
                          user=dbuser, password=dbpassword)
        cur: Cursor = con.cursor()
        logger.debug("Connected to DB "+dbdsn)
        cur.execute("SELECT * FROM TBL_ANIMAL WHERE DELETED<>1")
        rows = cur.fetchallmap()
        logger.debug("Count of DB rows: "+str(cur.rowcount))
        for row in rows:
            logger.debug("AnimalNr:"+str(row["animalnr"]))
            d = Animal()
            d.InitFromDB(row)
            if d.name == None:
                d.name = str(row["animalnr"])
            d.speciesID = config_object["SPECIESID"]["goat"]
            t = AnimalTransfer()
            t.animal = d;
            t.remoteID = str(row["intkey"])
            t.cloudID = ""
            t.updatePropertyList = "Name,ParamSelect10,AnimalNr,DateOfBirth,ParamString10,SpeciesID"
            synchMap = None
            for synch in animalSynchMap:
                if synch.intkey == str(row["intkey"]):
                    t.cloudID = synch.cloudID
                    synchMap = synch
                    break
            j = t.toJSON()
            logger.debug(j)
            r = requests.post(url, data=j, headers=headers)
            if r.status_code==200:
                tres = AnimalTransfer()
                tres.InitFromJSON(r.json())
                if synchMap is None:
                    synchMap = AnimalSynchMap(str(row["intkey"]), tres.cloudID, datetime.now(timezone.utc).isoformat())
                    animalSynchMap.append(synchMap)
                else:
                    synchMap.lastSynch = datetime.now(timezone.utc).isoformat()
                logger.debug(synchMap)
            else:
                logger.error(r.content)

        #save SynchMap data back to file
        animalsynchmap_json = json.dumps([ob.__dict__ for ob in animalSynchMap], default=lambda o: o.__dict__,
                          sort_keys=True, indent=2)
        logger.debug("AnymalSynchMap write to file: " + animalSynchDataFile)
        logger.debug("AnimalSynchMap: "+animalsynchmap_json)
        with open(animalSynchDataFile, "w+") as json_data:
            json_data.write(animalsynchmap_json)
    except BaseException as err:
        logger.error(f"Unexpected {err=}, {type(err)=}")

def AnimalDownload(config_object):
    try:
        logger.debug("***************")
        farminfo = config_object["FARMINFO"]
        speciesid = config_object["SPECIESID"]
        transfer = config_object["TRANSFER"]
        token = farminfo["token"]
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain', 'Authorization': 'Bearer ' + token}

        dbdsn = config_object["DBCONFIG"]["dsn"]
        dbuser = config_object["DBCONFIG"]["user"]
        dbpassword = config_object["DBCONFIG"]["password"]
        fdb.load_api()
        con = fdb.connect(dsn=dbdsn,
                          user=dbuser, password=dbpassword)
        cur: Cursor = con.cursor()

        animalTransfer = AnimalWithUpdate()
        animalTransfer.lastSynch = transfer["lastanimalsynch"]
        animalTransfer.speciesID = speciesid["goat"]
        animalTransfer.femaleOnly = False

        serverurl = config_object["SERVERCONFIG"]["url"]+config_object["SERVERCONFIG"]["animaldownload"]

        r = requests.post(serverurl, data=json.dumps(animalTransfer.__dict__), headers=headers)
        logger.debug(f"HTTP Status:{r.status_code}")
        if r.status_code == 200:
            for animalJson in r.json():
                a = Animal()
                a.InitFromJSON(animalJson)
                res = a.GetJSON()
                logger.debug(f"Animal changed: {a.paramSelect10} {a.id}")
                if AnimalNrFound(a.paramSelect10, con):
                    logger.debug(f"Animal {a.paramSelect10} found, update the database")
                    cur.execute("UPDATE TBL_ANIMAL "\
                                f"SET NAME='{a.name}',"\
                                f"MODBY='ifarmhousetransfer'"\
                                f"WHERE ANIMALNR='{a.paramSelect10}';")
                else:
                    logger.debug(f"Animal {a.paramSelect10} not found, insert the database")
                    cur.execute("INSERT INTO TBL_ANIMAL (ANIMALNR, NAME, MODBY) " \
                                f"VALUES('{a.paramSelect10}', '{a.name}', 'ifarmhousetransfer');")


            a_datetime = datetime.now(timezone.utc)
            formatted_datetime = a_datetime.isoformat()
            transfer["lastanimalsynch"] = formatted_datetime
        else:
            logger.warn(f"HTTP Status code {r.status_code} - {r.reason}")
    except BaseException as err:
        logger.error(f"Unexpected {err=}, {type(err)=}")

def AnimalNrFound(animalNr, dbCon):
    try:
        cur: Cursor = dbCon.cursor()
        query = f"SELECT intkey FROM TBL_ANIMAL WHERE animalnr={animalNr}"
        cur.execute(query)
        rows = cur.fetchall()
        logger.debug(f"Animal {animalNr} count {rows.count()}")
        return rows.count()>0;
    except BaseException as err:
        logger.error(f"Unexpected {err=}, {type(err)=}")
