class Device:
    farmID = ""
    localIP = ""
    internetIP = ""
    deviceID = ""
    hostname = ""
    memFree = -1
    memAvailable = -1
    memTotal = -1
    diskFree = -1
    diskTotal = -1
    cpuTemperatur = 0
    upTime = 0
    serialnumber = ""

    def __init__(self, farmID=None, localIP=None, internetIP=None, deviceID=None, hostname=None, lastPing=None,
                 token=None, id=None, lastMod=None, cpuTemperature=None, memFree=None, memAvailable=None, memTotal=None,
                 diskFree=None, diskTotal=None, upTime=None, serialnumber=None):
        if hostname: self.hostname = hostname
        if internetIP: self.internetIP = internetIP
        if localIP: self.localIP = localIP
        if farmID: self.farmID = farmID
        if deviceID: self.deviceID = deviceID
        if cpuTemperature: self.cpuTemperature = cpuTemperature
        if upTime: self.upTime = upTime
        if memFree: self.memFree = memFree
        if memAvailable: self.memAvailable = memAvailable
        if memTotal: self.memTotal = memTotal
        if diskFree: self.diskFree = diskFree
        if diskTotal: self.diskTotal = diskTotal
        if serialnumber: self.serialnumber = serialnumber


