import DeviceUpdate
import AnimalSynch
import logging
from logging.handlers import RotatingFileHandler

from configparser import ConfigParser
from time import sleep, gmtime
from datetime import datetime, timezone
from pathlib import Path

import sys

fileHandler = logging.handlers.RotatingFileHandler(
    filename='/ifarmhouse/log/foo.log',
    mode='a',
    maxBytes=5*1024*1024,
    backupCount=2,
    encoding=None,
    delay=0
)

consoleHandler = logging.StreamHandler()

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s %(levelname)s %(funcName)s(%(lineno)d) %(message)s",
    datefmt="%y-%m-%d %H:%M:%S",
    handlers=[
        fileHandler,
        consoleHandler
    ]
)

logger = logging.getLogger('main')

logger.info("******************************")
logger.info("* Startup iFarmhouseTransfer *")
logger.info("******************************")

headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}

configfile = "/ifarmhouse/config/config_prod.ini"

while True:
    # Read config.ini file
    config_object = ConfigParser()
    config_object.read(configfile)
    serverconfig = config_object["SERVERCONFIG"]
    url = serverconfig["url"]
    transfer = config_object["TRANSFER"]

    logger.info("Ping the server")
    DeviceUpdate.pingserver(url, config_object)

    logger.info("Get DeviceInfo")
    DeviceUpdate.getdeviceinfo(url, config_object)

    logger.info("Get Animals")
    AnimalSynch.AnimalDownload(config_object)

    logger.info("Update Animals")
    AnimalSynch.AnimalUpload(config_object)

    logger.debug("Update the timestamps")
    now = datetime.now()  # current date and time
    transfer["lastsynch"] = str(now)



    # Write changes back to file
    with open(configfile, 'w') as conf:
        config_object.write(conf)

    sleeptime = int(transfer["sleeptime"])
    logger.info("Go to sleep for "+str(sleeptime)+"sec")

    sys.stdout.flush()
    sleep(sleeptime)