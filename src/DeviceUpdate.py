import json
import socket
import requests
import logging
import psutil
import time
from requests import get
from Device import Device

logger = logging.getLogger('DeviceUpdate')

def pingserver(url, config_object):
    try:
        farminfo = config_object["FARMINFO"]
        token = farminfo["token"]
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain', 'Authorization': 'Bearer ' + token}

        try:
            hostname = socket.gethostname()
            local_ip = socket.gethostbyname(hostname)
        except Exception:
            local_ip = "no local ip"
        logger.debug("Local IP: "+local_ip)

        try:
            memory = psutil.virtual_memory()
            memtotal = memory.total
            memfree = memory.free
            memavailable = memory.available
        except Exception as err:
            logger.error(f"Access hardware with psutil:  {err=}, {type(err)=}")
            memtotal = -1
            memfree = -1
            memavailable = -1

        try:
            diskusage = psutil.disk_usage('/')
            disktotal = diskusage.total
            diskfree = diskusage.free
        except Exception as err:
            logger.error(f"Access hardware with psutil:  {err=}, {type(err)=}")
            disktotal = -1
            diskfree = -1

        try:
            sensors = psutil.sensors_temperatures()
            cpu_thermal = sensors["cpu_thermal"]
            cpu_temp = round(cpu_thermal[0].current)
        except Exception as err:
            logger.error(f"Access hardware with psutil:  {err=}, {type(err)=}")
            cpu_temp = 0

        logger.debug("CPU temperature: "+str(cpu_temp))

        logger.debug("Raspberry serial: "+getserial())
        logger.debug("Free Memory: " + getfreemem())
        #logger.debug("psutil memory: "+str(psutil.virtual_memory()))
        #logger.debug("psutil temperature: "+str(psutil.sensors_temperatures()))
        #logger.debug("psutil disk usage: "+str(psutil.disk_usage('/')))

        try:
            internet_ip = get('https://api.ipify.org').content.decode('utf8')
        except Exception as err:
            internet_ip = "no internet connection"
        logger.debug("Internet IP: " + internet_ip)

        logger.debug('My public IP address is: {}'.format(internet_ip))
        logger.debug("Local IP: " + local_ip)

        d = Device()
        d.localIP = local_ip
        d.internetIP = internet_ip
        d.farmID = farminfo["farmid"]
        d.deviceID = farminfo["deviceid"]
        d.hostname = hostname
        d.memFree  = memfree
        d.memAvailable = memavailable
        d.memTotal = memtotal
        d.cpuTemperature = cpu_temp
        d.diskFree = diskfree
        d.diskTotal = disktotal
        d.serialnumber = getserial()
        d.upTime = round(time.time() - psutil.boot_time())

        j = json.dumps(d.__dict__)
        logger.debug(j)
        serverurl = url + "/device/ping"
        logger.debug("Call server: "+serverurl)
        r = requests.post(serverurl, data=j, headers=headers)
        logger.debug("Ping response: "+str(r))
    except BaseException as err:
        logger.error(f"Unexpected {err=}, {type(err)=}")

def getdeviceinfo(url, config_object):
    try:
        farminfo = config_object["FARMINFO"]
        token = farminfo["token"]
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain', 'Authorization': 'Bearer '+token}

        serverurl = url + "/device/getdevicedetail"
        r = requests.get(serverurl, headers=headers)
        if r.status_code == 200:
            info_dict = r.json()
            logger.debug(info_dict["deviceID"])
            info =Device(**info_dict)
            logger.debug("Info:"+info.deviceID)
        else:
            logger.warn(f"HTTP Status code {r.status_code} - {r.reason}")
    except BaseException as err:
        logger.error(f"Unexpected {err=}, {type(err)=}")


def getserial():
    # Extract serial from cpuinfo file
    cpuserial = "0000000000000000"
    try:
        f = open('/proc/cpuinfo', 'r')
        for line in f:
            if line[0:6] == 'Serial':
                cpuserial = line[10:26]
        f.close()
    except:
        cpuserial = "ERROR000000000"

    return cpuserial

def getfreemem():
    # Extract serial from cpuinfo file
    freemem = "0"
    try:
        f = open('/proc/meminfo', 'r')
        for line in f:
            if line[0:7] == 'MemFree':
                freemem = line[17:24]
        f.close()
    except:
        freemem = "ERROR0"

    return freemem

