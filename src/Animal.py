from datetime import datetime

class Animal:
    id = ""
    animalNr = ""
    name = ""
    fatherID = ""
    fatherNr = ""
    motherID = ""
    motherNr = ""
    dateOfBirth = ""
    speciesID = ""
    farmID = "00000000-0000-0000-0000-000000000001"
    lastMod = "2021-06-21T12:21:43.3456057Z"
    paramSelect10 = 0
    paramString10 = ""

    def InitFromJSON(self, animalJSON):
        self.id = animalJSON["id"]
        self.paramSelect10 = animalJSON["paramSelect10"]
        self.name = animalJSON["name"]
        self.fatherID = animalJSON["fatherID"]
        self.fatherNr = animalJSON["fatherNr"]
        self.motherID = animalJSON["motherID"]
        self.motherNr = animalJSON["motherNr"]
        self.dateOfBirth = animalJSON["dateOfBirth"]
        self.speciesID = animalJSON["speciesID"]
        self.farmID = animalJSON["farmID"]
        self.lastMod = animalJSON["lastMod"]

    def InitFromDB(self, row):
        self.id = ''
        self.paramSelect10 = row["animalnr"]
        self.paramString10 = str(row["animalnr"])
        self.name = row["name"]
        self.animalNr = str(row["irnr"])
        self.dateOfBirth = datetime.strptime(str(row["date_birth"]), "%Y-%m-%d").isoformat()

    def GetJSON(self):
        res = {}
        res["id"] = self.id
        res["name"] = self.name
        res["paramSelect10"] = self.paramSelect10
        return res
