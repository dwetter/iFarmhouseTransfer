#!/bin/bash

# turn on bash's job control
set -m

firebird &

/ifarmhouse/db/start_fb.sh

fg %1