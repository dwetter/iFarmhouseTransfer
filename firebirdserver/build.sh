#!/bin/bash

FBINSTFILE=$(basename ${FBURL})
FBINSTDIR=$(basename -s .tar.gz ${FBURL})

echo ******************************************
echo ********* INSTALL FIREBIRD ***************
echo ******************************************

if [ ! -d "/tmp/inst/firebird" ]; then
  if [ ! -d "/tmp/inst" ]; then
    echo mkdir /tmp/inst
    mkdir /tmp/inst
  fi
  echo mkdir /tmp/inst/firebird
  mkdir /tmp/inst/firebird
fi

echo cd /tmp/inst/firebird
cd /tmp/inst/firebird

if [ -f "/tmp/inst/firebird/Firebird-withDebugInfo-4.0.0.2496-0.arm.tar.gz" ]; then
  echo rm /tmp/inst/firebird/Firebird-withDebugInfo-4.0.0.2496-0.arm.tar.gz
  rm /tmp/inst/firebird/Firebird-withDebugInfo-4.0.0.2496-0.arm.tar.gz
fi

echo download the firebird from ${FBURL}
wget -nv ${FBURL} --no-check-certificate

echo tar -xzvf ${FBINSTFILE} -C /opt
tar -xzvf ${FBINSTFILE} -C /opt

echo chmod -R 777 /opt/firebird
chmod -R 777 /opt/firebird

echo rm -Rf /tmp/inst/firebird
rm -Rf /tmp/inst/firebird

export PATH="/opt/firebird/bin:$PATH"

mkdir /usr/local/firebird

echo ******************************************
echo ********** INSTALL DONE  *****************
echo ******************************************



