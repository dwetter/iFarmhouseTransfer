﻿FROM arm32v7/debian:bookworm

LABEL maintainer="daniel@wetter.ru"
LABEL version="4.0.0.2496"
LABEL vendor=""
LABEL release-date="2021-11-15"

RUN apt-get update
RUN apt-get install -qy wget
RUN apt-get install -y procps
RUN apt-get install -y net-tools
RUN apt-get install -y vim
#RUN apt-get install -qy firebird3.0-server
#RUN apt-get install -qy firebird3.0-examples firebird-dev

RUN apt-get install -y python3
RUN apt-get install -y python3-pip
RUN pip install fdb
RUN pip install requests
RUN pip install configparser
RUN pip install psutil
RUN apt-get update
RUN apt-get install -y net-tools
RUN apt-get install -y libfbclient2

RUN mkdir /ifarmhouse
RUN mkdir /ifarmhouse/log

#RUN mkdir /ifarmhouse/db
#COPY ./firebirdserver/CowDB_Backup.sql /ifarmhouse/db
#COPY ./firebirdserver/start_fb.sh /ifarmhouse/db
#RUN chmod +x /ifarmhouse/db/start_fb.sh

#COPY ./process_wrapper.sh .
#RUN chmod +x process_wrapper.sh

#EXPOSE 3050/tcp
#EXPOSE 3051/tcp

#CMD ./process_wrapper.sh
ENTRYPOINT ["python3", "/ifarmhouse/src/main.py"]